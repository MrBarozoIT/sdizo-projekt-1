#pragma once
#include <iostream>
class Tablica
{
private:

	int rozmiar;
	int* glowa;
	
	void zwiekszRozmiar();
	void zmniejszRozmiar();

	void dodaj(int wartosc, int index);
	void usun(int index);
	bool wyszukaj(int wartosc);
public:
	Tablica();
	~Tablica();

	int getRozmiar();
	int getElement(int index);
	void wyswietl();
	
	void dodajNaPoczatek(int wartosc);
	void dodajNaKoniec(int wartosc);
	void dodajNaIndeks(int wartosc, int index);

	void usunZPoczatek();
	void usunZKoniec();
	void usunZIndeks(int index);

	void znajdzWartosc(int wartosc);
};

constexpr int pusta = 0;
