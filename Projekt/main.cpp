#include <iostream>

//--------- Pliki do��czone do projektu: --------- 
#include "H/Tablica.h"

int main() 
{
	std::cout << "SDiZO ---> Projekt 1 " << std::endl;

	Tablica tablica;
	for (auto i = 0; i < 5; i++)
	{
		tablica.dodajNaPoczatek(i);
	}
	tablica.wyswietl();
	std::cout << std::endl << std::endl;
	
	for (auto i = 0; i < 5; i++)
	{
		tablica.dodajNaKoniec(i);
	}
	tablica.wyswietl();
	std::cout << std::endl << std::endl;

		tablica.dodajNaIndeks(6,2);
		tablica.wyswietl();
		std::cout << std::endl << std::endl;
		tablica.dodajNaIndeks(7, 4);
		tablica.wyswietl();
		std::cout << std::endl << std::endl;
		tablica.dodajNaIndeks(8, 6);
		tablica.wyswietl();
		std::cout << std::endl << std::endl;

		tablica.usunZPoczatek();
		tablica.wyswietl();
		std::cout << std::endl << std::endl;

		tablica.usunZPoczatek();
		tablica.wyswietl();
		std::cout << std::endl << std::endl;

		tablica.usunZPoczatek();
		tablica.wyswietl();
		std::cout << std::endl << std::endl;

		tablica.usunZPoczatek();
		tablica.wyswietl();
		std::cout << std::endl << std::endl;
	
		tablica.znajdzWartosc(7);
		tablica.znajdzWartosc(8);
		tablica.znajdzWartosc(1);
	return 0;
}