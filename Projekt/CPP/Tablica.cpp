#include "../H/Tablica.h"

int Tablica::getRozmiar()
{
	return rozmiar;
}

int Tablica::getElement(int index)
{
	if (index <= getRozmiar() and index >= pusta) 
	{
		return glowa[index];
	}
	else
	{
		std::cout << "Z�y indeks!" << std::endl;
		return -1;
	}
}

void Tablica::zwiekszRozmiar()
{
	++rozmiar;
}

void Tablica::zmniejszRozmiar()
{
	--rozmiar;
}

Tablica::Tablica()
{
	glowa = nullptr;
	rozmiar = pusta;
}

Tablica::~Tablica()
{
	delete[] glowa;
}

void Tablica::wyswietl()
{
	if (getRozmiar() == pusta) 
	{
		std::cout << "Tablica pusta!" << std::endl;
	}
	else
	{
		for (auto i = 0; i < getRozmiar(); i++)
		{
			std::cout<<"["<<i<<":] "<< glowa[i] << '\t';
		}
	}
	
}

void Tablica::dodajNaPoczatek(int wartosc)
{
	dodaj(wartosc, 0);
}

void Tablica::dodajNaKoniec(int wartosc)
{
	dodaj(wartosc, getRozmiar());
}

void Tablica::dodajNaIndeks(int wartosc, int index)
{
	dodaj(wartosc, index);
}

void Tablica::usunZPoczatek()
{
	usun(0);
}

void Tablica::usunZKoniec()
{
	usun(getRozmiar());
}

void Tablica::usunZIndeks(int index)
{
	usun(index);
}

void Tablica::znajdzWartosc(int wartosc)
{	
	if (wyszukaj(wartosc))
	{
		std::cout << "Znaleziono wartosc: " << wartosc << std::endl;
	}
	else
	{
		std::cout << "Nie znaleziono wartosc: " << wartosc << std::endl;
	}
}



void Tablica::dodaj(int wartosc, int index)
{
	if (getRozmiar() == pusta)
	{
		glowa = new int[1];
		*glowa = wartosc;
		zwiekszRozmiar();
	}
	else if (index >= 0 and index < getRozmiar() + 1)
	{
		auto* tymczasowa = new int[getRozmiar() + 1];
		for (auto i = 0; i < index; i++)
		{
			tymczasowa[i] = glowa[i];
		}		
		for (auto i = index; i < getRozmiar(); i++)
		{
			tymczasowa[i+1] = glowa[i];
		}
		tymczasowa[index] = wartosc;
		delete[] glowa;
		glowa = tymczasowa;
		zwiekszRozmiar();
	}
	else if(index>=getRozmiar()+1)
	{
		auto* tymczasowa = new int[getRozmiar() + 1];
		tymczasowa[getRozmiar()] = wartosc;
		for (auto i = 0; i < getRozmiar(); i++)
		{
			tymczasowa[i] = glowa[i];
		}
		delete[] glowa;
		glowa = tymczasowa;
		zwiekszRozmiar();
	}
}

void Tablica::usun(int index)
{
	if (getRozmiar()==pusta)
	{
		std::cout << "Tablica pusta!" << std::endl;
		return;
	}
	else
	{
		auto* tymczasowa = new int[getRozmiar()];
		for (auto i = 0; i < index; i++)
		{
			tymczasowa[i] = glowa[i];
		}
		for (auto i = index; i < getRozmiar(); i++)
		{
			tymczasowa[i] = glowa[i+1];
		}
		delete[] glowa;
		glowa = tymczasowa;
		zmniejszRozmiar();
	}
}

bool Tablica::wyszukaj(int wartosc)
{
	for (auto i = 0; i < getRozmiar(); i++)
	{
		if (wartosc == glowa[i])
		{
			return true;
		}
	}
	return false;
}
